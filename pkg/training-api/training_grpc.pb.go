// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.20.0--rc1
// source: training.proto

package trainingapi

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// TrainingServiceClient is the client API for TrainingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type TrainingServiceClient interface {
	// Запрос ответ
	CreateNotification(ctx context.Context, in *CreateNotificationRequest, opts ...grpc.CallOption) (*CreateNotificationResponse, error)
	// Стрим со стороны сервера
	ListNotifications(ctx context.Context, in *ListNotificationsRequest, opts ...grpc.CallOption) (TrainingService_ListNotificationsClient, error)
	// Стрим со стороны клиента
	TakingOrders(ctx context.Context, opts ...grpc.CallOption) (TrainingService_TakingOrdersClient, error)
	// Двусторонний стрим
	RouteEcho(ctx context.Context, opts ...grpc.CallOption) (TrainingService_RouteEchoClient, error)
}

type trainingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewTrainingServiceClient(cc grpc.ClientConnInterface) TrainingServiceClient {
	return &trainingServiceClient{cc}
}

func (c *trainingServiceClient) CreateNotification(ctx context.Context, in *CreateNotificationRequest, opts ...grpc.CallOption) (*CreateNotificationResponse, error) {
	out := new(CreateNotificationResponse)
	err := c.cc.Invoke(ctx, "/mediasoft.internship.season2023.lesson8.training.TrainingService/CreateNotification", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *trainingServiceClient) ListNotifications(ctx context.Context, in *ListNotificationsRequest, opts ...grpc.CallOption) (TrainingService_ListNotificationsClient, error) {
	stream, err := c.cc.NewStream(ctx, &TrainingService_ServiceDesc.Streams[0], "/mediasoft.internship.season2023.lesson8.training.TrainingService/ListNotifications", opts...)
	if err != nil {
		return nil, err
	}
	x := &trainingServiceListNotificationsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type TrainingService_ListNotificationsClient interface {
	Recv() (*Notification, error)
	grpc.ClientStream
}

type trainingServiceListNotificationsClient struct {
	grpc.ClientStream
}

func (x *trainingServiceListNotificationsClient) Recv() (*Notification, error) {
	m := new(Notification)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *trainingServiceClient) TakingOrders(ctx context.Context, opts ...grpc.CallOption) (TrainingService_TakingOrdersClient, error) {
	stream, err := c.cc.NewStream(ctx, &TrainingService_ServiceDesc.Streams[1], "/mediasoft.internship.season2023.lesson8.training.TrainingService/TakingOrders", opts...)
	if err != nil {
		return nil, err
	}
	x := &trainingServiceTakingOrdersClient{stream}
	return x, nil
}

type TrainingService_TakingOrdersClient interface {
	Send(*Order) error
	CloseAndRecv() (*TakingOrdersResponse, error)
	grpc.ClientStream
}

type trainingServiceTakingOrdersClient struct {
	grpc.ClientStream
}

func (x *trainingServiceTakingOrdersClient) Send(m *Order) error {
	return x.ClientStream.SendMsg(m)
}

func (x *trainingServiceTakingOrdersClient) CloseAndRecv() (*TakingOrdersResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(TakingOrdersResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *trainingServiceClient) RouteEcho(ctx context.Context, opts ...grpc.CallOption) (TrainingService_RouteEchoClient, error) {
	stream, err := c.cc.NewStream(ctx, &TrainingService_ServiceDesc.Streams[2], "/mediasoft.internship.season2023.lesson8.training.TrainingService/RouteEcho", opts...)
	if err != nil {
		return nil, err
	}
	x := &trainingServiceRouteEchoClient{stream}
	return x, nil
}

type TrainingService_RouteEchoClient interface {
	Send(*EchoMessage) error
	Recv() (*EchoMessage, error)
	grpc.ClientStream
}

type trainingServiceRouteEchoClient struct {
	grpc.ClientStream
}

func (x *trainingServiceRouteEchoClient) Send(m *EchoMessage) error {
	return x.ClientStream.SendMsg(m)
}

func (x *trainingServiceRouteEchoClient) Recv() (*EchoMessage, error) {
	m := new(EchoMessage)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// TrainingServiceServer is the server API for TrainingService service.
// All implementations must embed UnimplementedTrainingServiceServer
// for forward compatibility
type TrainingServiceServer interface {
	// Запрос ответ
	CreateNotification(context.Context, *CreateNotificationRequest) (*CreateNotificationResponse, error)
	// Стрим со стороны сервера
	ListNotifications(*ListNotificationsRequest, TrainingService_ListNotificationsServer) error
	// Стрим со стороны клиента
	TakingOrders(TrainingService_TakingOrdersServer) error
	// Двусторонний стрим
	RouteEcho(TrainingService_RouteEchoServer) error
	mustEmbedUnimplementedTrainingServiceServer()
}

// UnimplementedTrainingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedTrainingServiceServer struct {
}

func (UnimplementedTrainingServiceServer) CreateNotification(context.Context, *CreateNotificationRequest) (*CreateNotificationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateNotification not implemented")
}
func (UnimplementedTrainingServiceServer) ListNotifications(*ListNotificationsRequest, TrainingService_ListNotificationsServer) error {
	return status.Errorf(codes.Unimplemented, "method ListNotifications not implemented")
}
func (UnimplementedTrainingServiceServer) TakingOrders(TrainingService_TakingOrdersServer) error {
	return status.Errorf(codes.Unimplemented, "method TakingOrders not implemented")
}
func (UnimplementedTrainingServiceServer) RouteEcho(TrainingService_RouteEchoServer) error {
	return status.Errorf(codes.Unimplemented, "method RouteEcho not implemented")
}
func (UnimplementedTrainingServiceServer) mustEmbedUnimplementedTrainingServiceServer() {}

// UnsafeTrainingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to TrainingServiceServer will
// result in compilation errors.
type UnsafeTrainingServiceServer interface {
	mustEmbedUnimplementedTrainingServiceServer()
}

func RegisterTrainingServiceServer(s grpc.ServiceRegistrar, srv TrainingServiceServer) {
	s.RegisterService(&TrainingService_ServiceDesc, srv)
}

func _TrainingService_CreateNotification_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateNotificationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TrainingServiceServer).CreateNotification(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/mediasoft.internship.season2023.lesson8.training.TrainingService/CreateNotification",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TrainingServiceServer).CreateNotification(ctx, req.(*CreateNotificationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _TrainingService_ListNotifications_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(ListNotificationsRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(TrainingServiceServer).ListNotifications(m, &trainingServiceListNotificationsServer{stream})
}

type TrainingService_ListNotificationsServer interface {
	Send(*Notification) error
	grpc.ServerStream
}

type trainingServiceListNotificationsServer struct {
	grpc.ServerStream
}

func (x *trainingServiceListNotificationsServer) Send(m *Notification) error {
	return x.ServerStream.SendMsg(m)
}

func _TrainingService_TakingOrders_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(TrainingServiceServer).TakingOrders(&trainingServiceTakingOrdersServer{stream})
}

type TrainingService_TakingOrdersServer interface {
	SendAndClose(*TakingOrdersResponse) error
	Recv() (*Order, error)
	grpc.ServerStream
}

type trainingServiceTakingOrdersServer struct {
	grpc.ServerStream
}

func (x *trainingServiceTakingOrdersServer) SendAndClose(m *TakingOrdersResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *trainingServiceTakingOrdersServer) Recv() (*Order, error) {
	m := new(Order)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _TrainingService_RouteEcho_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(TrainingServiceServer).RouteEcho(&trainingServiceRouteEchoServer{stream})
}

type TrainingService_RouteEchoServer interface {
	Send(*EchoMessage) error
	Recv() (*EchoMessage, error)
	grpc.ServerStream
}

type trainingServiceRouteEchoServer struct {
	grpc.ServerStream
}

func (x *trainingServiceRouteEchoServer) Send(m *EchoMessage) error {
	return x.ServerStream.SendMsg(m)
}

func (x *trainingServiceRouteEchoServer) Recv() (*EchoMessage, error) {
	m := new(EchoMessage)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// TrainingService_ServiceDesc is the grpc.ServiceDesc for TrainingService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var TrainingService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "mediasoft.internship.season2023.lesson8.training.TrainingService",
	HandlerType: (*TrainingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateNotification",
			Handler:    _TrainingService_CreateNotification_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "ListNotifications",
			Handler:       _TrainingService_ListNotifications_Handler,
			ServerStreams: true,
		},
		{
			StreamName:    "TakingOrders",
			Handler:       _TrainingService_TakingOrders_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "RouteEcho",
			Handler:       _TrainingService_RouteEcho_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "training.proto",
}
