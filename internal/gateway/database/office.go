package database

import (
	"sync"
	"time"

	"github.com/google/uuid"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/gateway/model"
)

type OfficeInMem struct {
	mu      sync.Mutex
	offices []*model.Office
}

func NewOfficeInMem() *OfficeInMem {
	return &OfficeInMem{
		mu:      sync.Mutex{},
		offices: make([]*model.Office, 0, 32),
	}
}

func (mem *OfficeInMem) Add(office *model.Office) {
	mem.mu.Lock()
	defer mem.mu.Unlock()
	office.CreatedAt = time.Now()
	office.UUID, _ = uuid.NewUUID()
	mem.offices = append(mem.offices, office)
}

func (mem *OfficeInMem) GetList() []*model.Office {
	mem.mu.Lock()
	defer mem.mu.Unlock()
	newOffices := make([]*model.Office, len(mem.offices))
	copy(newOffices, mem.offices)
	return newOffices
}
