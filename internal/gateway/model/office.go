package model

import (
	"time"

	"github.com/google/uuid"
)

type Office struct {
	UUID          uuid.UUID
	Name, Address string
	CreatedAt     time.Time
}
