package service

import (
	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/gateway/database"
	gatewayapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/gateway-api"
)

type Service struct {
	gatewayapi.UnimplementedOfficeServiceServer

	officeMem *database.OfficeInMem
}

func New() *Service {
	return &Service{
		officeMem: database.NewOfficeInMem(),
	}
}
