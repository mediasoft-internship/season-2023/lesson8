package service

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/gateway/model"
	gatewayapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/gateway-api"
)

func (s *Service) CreateOffice(
	ctx context.Context, request *gatewayapi.CreateOfficeRequest,
) (*gatewayapi.CreateOfficeResponse, error) {
	if err := request.ValidateAll(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	s.officeMem.Add(&model.Office{
		Name:    request.Name,
		Address: request.Address,
	})

	return &gatewayapi.CreateOfficeResponse{}, nil
}
