package service

import (
	"context"

	"google.golang.org/protobuf/types/known/timestamppb"

	gatewayapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/gateway-api"
)

func (s *Service) GetOfficeList(
	ctx context.Context, request *gatewayapi.GetOfficeListRequest,
) (*gatewayapi.GetOfficeListResponse, error) {

	memOffices := s.officeMem.GetList()
	apiOffices := make([]*gatewayapi.Office, 0, len(memOffices))
	for _, office := range s.officeMem.GetList() {
		apiOffices = append(apiOffices, &gatewayapi.Office{
			Uuid:      office.UUID.String(),
			Name:      office.Name,
			Address:   office.Address,
			CreatedAt: timestamppb.New(office.CreatedAt),
		})
	}

	return &gatewayapi.GetOfficeListResponse{
		Result: apiOffices,
	}, nil
}
