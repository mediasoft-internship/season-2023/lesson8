package database

import (
	"sync"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/model"
)

type NotificationInMem struct {
	mu            sync.Mutex
	notifications []*model.Notification
}

func NewNotificationInMem() *NotificationInMem {
	return &NotificationInMem{
		mu:            sync.Mutex{},
		notifications: make([]*model.Notification, 0, 32),
	}
}

func (mem *NotificationInMem) Add(notificaton *model.Notification) {
	mem.mu.Lock()
	defer mem.mu.Unlock()
	mem.notifications = append(mem.notifications, notificaton)
}

func (mem *NotificationInMem) GetList() []*model.Notification {
	mem.mu.Lock()
	defer mem.mu.Unlock()
	newNotifications := make([]*model.Notification, len(mem.notifications))
	copy(newNotifications, mem.notifications)
	return newNotifications
}
