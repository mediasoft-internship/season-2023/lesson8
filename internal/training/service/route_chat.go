package service

import (
	"errors"
	"io"

	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func (s *Service) RouteEcho(
	echoServer trainingapi.TrainingService_RouteEchoServer,
) error {

	messageChan := make(chan *trainingapi.EchoMessage, 16)
	errorsChan := make(chan error, 1)
	go func() {
		for {
			message, err := echoServer.Recv()
			if errors.Is(err, io.EOF) {
				close(messageChan)
				return
			}
			if err != nil {
				errorsChan <- err
				return
			}
			messageChan <- message
		}
	}()

	for {
		select {
		case err := <-errorsChan:
			return err
		case message, ok := <-messageChan:
			if !ok {
				return nil
			}
			if err := echoServer.Send(message); err != nil {
				return err
			}
		}
	}
}
