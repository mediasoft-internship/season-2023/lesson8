package service

import (
	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/database"
	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

type Service struct {
	trainingapi.UnimplementedTrainingServiceServer

	notificationMem *database.NotificationInMem
}

func New() *Service {
	return &Service{
		notificationMem: database.NewNotificationInMem(),
	}
}
