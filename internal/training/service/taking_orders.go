package service

import (
	"errors"
	"io"
	"log"

	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func (s *Service) TakingOrders(
	ordersServer trainingapi.TrainingService_TakingOrdersServer,
) error {
	for {
		order, err := ordersServer.Recv()
		if errors.Is(err, io.EOF) {
			return ordersServer.SendAndClose(&trainingapi.TakingOrdersResponse{})
		}
		if err != nil {
			return err
		}
		log.Println("order received: ", order)
	}
}
