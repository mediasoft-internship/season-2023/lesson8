package service

import (
	"time"

	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func (s *Service) ListNotifications(
	request *trainingapi.ListNotificationsRequest,
	notificationsServer trainingapi.TrainingService_ListNotificationsServer,
) error {

	notifications := s.notificationMem.GetList()

	for _, n := range notifications {
		time.Sleep(time.Second)
		if err := notificationsServer.Send(&trainingapi.Notification{
			Author: n.Author,
			Body:   n.Body,
		}); err != nil {
			return err
		}
	}

	return nil
}
