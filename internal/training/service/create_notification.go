package service

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/model"
	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func (s *Service) CreateNotification(
	ctx context.Context, request *trainingapi.CreateNotificationRequest,
) (*trainingapi.CreateNotificationResponse, error) {
	notification := model.Notification{
		Author: request.Author,
		Body:   request.Body,
	}
	if err := notification.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	s.notificationMem.Add(&notification)
	return &trainingapi.CreateNotificationResponse{}, nil
}
