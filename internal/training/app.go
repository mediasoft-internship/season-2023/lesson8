package training

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/config"
	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/service"
	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func Run(cfg config.Config) error {

	ts := service.New()
	s := grpc.NewServer()
	trainingapi.RegisterTrainingServiceServer(s, ts)

	l, err := net.Listen("tcp", cfg.GRPCAddr)
	if err != nil {
		return fmt.Errorf("failed to listen tcp %s, %v", cfg.GRPCAddr, err)
	}

	go func() {
		log.Printf("starting listening grpc server at %s", cfg.GRPCAddr)
		if err := s.Serve(l); err != nil {
			log.Fatalf("error service grpc server %v", err)
		}
	}()

	gracefulShutDown(s)

	return nil
}

func gracefulShutDown(s *grpc.Server) {
	const waitTime = 5 * time.Second

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(ch)

	sig := <-ch
	errorMessage := fmt.Sprintf("%s %v - %s", "Received shutdown signal:", sig, "Graceful shutdown done")
	log.Println(errorMessage)
	s.GracefulStop()
}
