package model

import "errors"

type Notification struct {
	Author, Body string
}

func (n *Notification) Validate() error {
	if n.Author == "" {
		return errors.New("author required value")
	}
	if n.Body == "" {
		return errors.New("body required value")
	}
	return nil
}
