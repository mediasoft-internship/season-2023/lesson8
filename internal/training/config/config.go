package config

type Config struct {
	GRPCAddr string `env:"TRAINING_GRPC_ADDR" envDefault:":13999"`
}
