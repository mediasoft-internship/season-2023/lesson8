package main

import (
	"log"

	"github.com/caarlos0/env"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/gateway"
	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/gateway/config"
)

func main() {
	cfg := config.Config{}

	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("failed to retrieve env variables, %v", err)
	}

	if err := gateway.Run(cfg); err != nil {
		log.Fatal("error running gateway server ", err)
	}
}
