package main

import (
	"log"

	"github.com/caarlos0/env"

	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training"
	"gitlab.com/mediasoft-internship/season-2023/lesson8/internal/training/config"
)

func main() {
	cfg := config.Config{}

	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("failed to retrieve env variables, %v", err)
	}

	if err := training.Run(cfg); err != nil {
		log.Fatal("error running grpc server ", err)
	}
}
