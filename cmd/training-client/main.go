package main

import (
	"context"
	"errors"
	"io"
	"log"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	trainingapi "gitlab.com/mediasoft-internship/season-2023/lesson8/pkg/training-api"
)

func main() {

	conn, err := grpc.Dial(
		"0.0.0.0:13999",
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatal("error connect to grpc server err:", err)
	}
	defer conn.Close()

	client := trainingapi.NewTrainingServiceClient(conn)

	if err := createNotification(client); err != nil {
		log.Fatal(err)
	}
	if err := getNotifications(client); err != nil {
		log.Fatal(err)
	}
	if err := takingOrders(client); err != nil {
		log.Fatal(err)
	}
	if err := routeEcho(client); err != nil {
		log.Fatal(err)
	}

}

func createNotification(client trainingapi.TrainingServiceClient) error {

	notifications := []*trainingapi.CreateNotificationRequest{
		{
			Author: "auth1",
			Body:   "Hello World!!!",
		},
		{
			Author: "auth2",
			Body:   "Golang",
		},
		{
			Author: "auth1",
			Body:   "BLA-BLA-BLA-BLA)))",
		},
	}

	for _, n := range notifications {
		if _, err := client.CreateNotification(context.Background(), n); err != nil {
			return err
		}
		log.Println("notification created: ", n)
	}

	return nil
}

func getNotifications(client trainingapi.TrainingServiceClient) error {
	stream, err := client.ListNotifications(
		context.Background(), &trainingapi.ListNotificationsRequest{},
	)
	if err != nil {
		return err
	}

	for {
		notification, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			log.Println("read all notifications")
			return nil
		}
		if err != nil {
			return err
		}
		log.Println("read notification: ", notification)
	}
}

func takingOrders(client trainingapi.TrainingServiceClient) error {

	orders := []*trainingapi.Order{
		{
			Id:          1,
			ClientId:    1,
			ProductsIds: []int64{1, 2, 3, 4},
		},
		{
			Id:          2,
			ClientId:    2,
			ProductsIds: []int64{1, 4},
		},
		{
			Id:          3,
			ClientId:    5,
			ProductsIds: []int64{1, 3, 4},
		},
	}

	stream, err := client.TakingOrders(context.Background())
	if err != nil {
		return err
	}

	for _, o := range orders {
		time.Sleep(time.Second)
		if err := stream.Send(o); err != nil {
			return err
		}
		log.Println("send order: ", o)
	}
	_, err = stream.CloseAndRecv()
	log.Println("orders created: ")
	return err
}

func routeEcho(client trainingapi.TrainingServiceClient) error {
	stream, err := client.RouteEcho(context.Background())
	if err != nil {
		return err
	}

	messages := []string{
		"A monument I've raised not built with hands,",
		"And common folk shall keep the path well trodden",
		"To where it unsubdued and towering stands",
		"Higher than Alexander's Column.",
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			message, err := stream.Recv()
			if errors.Is(err, io.EOF) {
				log.Println("routeEcho(): read all message")
				return
			}
			if err != nil {
				log.Println("error while reading message: ", err)
				return
			}
			log.Println("routeEcho(): incoming message: ", message.Body)
		}
	}()

	for _, m := range messages {
		if err := stream.Send(&trainingapi.EchoMessage{
			Body: m,
		}); err != nil {
			return err
		}
		log.Println("routeEcho(): send message: ", m)
	}
	if err := stream.CloseSend(); err != nil {
		return err
	}
	wg.Wait()

	return nil
}
