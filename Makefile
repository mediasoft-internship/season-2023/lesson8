TRAINING_PROTO_OUT_DIR = pkg/training-api
TRAINING_API_PATH = api/mediasoft-internship/season-2023/lesson8/training

.PHONY: gen-training

gen-training:
	mkdir -p ${TRAINING_PROTO_OUT_DIR}
	protoc \
		-I ${TRAINING_API_PATH} \
		--go_out=$(TRAINING_PROTO_OUT_DIR) --go_opt=paths=source_relative \
		--go-grpc_out=$(TRAINING_PROTO_OUT_DIR) --go-grpc_opt=paths=source_relative \
		./${TRAINING_API_PATH}/*.proto


GATEWAY_PROTO_OUT_DIR = pkg/gateway-api
GATEWAY_API_PATH = api/mediasoft-internship/season-2023/lesson8/gateway


gen-gateway:
	mkdir -p ${GATEWAY_PROTO_OUT_DIR}
	protoc \
		-I ${GATEWAY_API_PATH} \
		-I third_party/googleapis \
		-I third_party/envoyproxy/protoc-gen-validate \
		--go_out=./$(GATEWAY_PROTO_OUT_DIR) --go_opt=paths=source_relative \
		--go-grpc_out=./$(GATEWAY_PROTO_OUT_DIR)  --go-grpc_opt=paths=source_relative \
		--validate_out="lang=go:./$(GATEWAY_PROTO_OUT_DIR)" --validate_opt=paths=source_relative \
		--grpc-gateway_out=./$(GATEWAY_PROTO_OUT_DIR) --grpc-gateway_opt=paths=source_relative \
		--openapiv2_out=use_go_templates=true,json_names_for_fields=false,allow_merge=true,merge_file_name=api:./$(GATEWAY_PROTO_OUT_DIR) \
		./${GATEWAY_API_PATH}/*.proto
